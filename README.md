# Base de données de ListesMunicipales.fr

Pour accéder aux données, composer l'url suivant ce schéma :

```
/:codeDepartement/:codeInseeCommune/:idListe.json
```