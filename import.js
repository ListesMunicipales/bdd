const request = require('request');
var path = require('path'),
    fs = require('fs');
const fse = require('fs-extra');


function callback(error, response, body) {
    var communes = JSON.parse(body);
    var index = {};

    function writeCommuneInfo(c) {
        if (communes[c]) {
            var dep = communes[c].codeDepartement;
            if (dep == undefined) {
                dep = "Autres";
            }
            var codeInsee = communes[c].code;
            if (index[dep] == undefined) {
                index[dep] = [];
            }
            index[dep].push(codeInsee);
            fse.outputFile("public/" + dep + "/" + codeInsee + "/info.json", JSON.stringify(communes[c]))
                .then(() => {
                    console.log(codeInsee);
                    writeCommuneInfo(c + 1);
                })
                .catch(err => {
                    console.error(err)
                });
        } else {
            fse.outputFile("public/index.json", JSON.stringify(index))
                .then(() => {
                    console.log("Terminé : " + (c + 1) + " communes importées !");
                })
                .catch(err => {
                    console.error(err)
                });
        }
    }
    writeCommuneInfo(0);
}

request("https://geo.api.gouv.fr/communes?fields=nom,code,codesPostaux,codeDepartement,codeRegion,population&format=json&geometry=centre", callback);